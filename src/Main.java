

import java.util.*;

public class Main {

    public static void main(String[] args) {
        //for my list I chose to use different foods

        /**
         * LIST
         */
        //output that this is a list example
        System.out.println("-----List Example-----");
        //create array
        List list = new ArrayList();

        //add values to list
        list.add("Cheeseburger");
        list.add("Burrito");
        list.add("French Fries");
        list.add("Pizza");
        list.add("Tacos");
        list.add("Chicken Nuggets");

        //loop to call values
        for (Object str : list) {
            System.out.println((String) str);
        }

        /**
         * Set
         */
        //output that this is a set example
        System.out.println("-----Set Example-----");
        //create tree set
        Set set = new TreeSet();

        //add values to set
        set.add("Cheeseburger");
        set.add("Burrito");
        set.add("French Fries");
        set.add("Pizza");
        set.add("Tacos");
        set.add("Chicken Nuggets");

        //loop to call values
        for (Object str : set){
            System.out.println((String) str);
        }

        /**
         * Queue
         */
        //output that this is a queue example
        System.out.println("-----Queue Example-----");
        //create queue
        Queue queue = new PriorityQueue();

        //add values to queue
        queue.add("Cheeseburger");
        queue.add("Burrito");
        queue.add("French Fries");
        queue.add("Pizza");
        queue.add("Tacos");
        queue.add("Chicken Nuggets");

        //loop to call values
        Iterator iterator = queue.iterator();
        while (iterator.hasNext()){
            System.out.println(queue.poll());
        }

        /**
         * Map
         */
        //output that this is a map example
        System.out.println("-----Map Example-----");
        //create map
        Map map = new HashMap();

        //add values to map
        map.put(1,"Cheeseburger");
        map.put(2,"Burrito");
        map.put(3,"French Fries");
        map.put(4,"Pizza");
        map.put(5,"Tacos");
        map.put(6,"Chicken Nuggets");

        //loop to call values
        for (int i = 1; i < 6; i++){
            String outcome = (String)map.get(i);
            System.out.println(outcome);
        }

        /**
         * Generic List
         */
        //output that this is a generic link list
        System.out.println("-----Generic List-----");
        List<VideoGames> games = new LinkedList<VideoGames>();
        games.add(new VideoGames("Skyrim ","Bethesda ", "2011"));
        games.add(new VideoGames("Mario Kart 8 ","Nintendo ", "2014"));
        games.add(new VideoGames("Uncharted ","Naughty Dog ", "2007"));
        games.add(new VideoGames("Metal Gear Solid ","Konami ", "1998"));
        games.add(new VideoGames("Kingdom Hearts ","Square Enix ", "2002"));

        //loop to call values
        for (VideoGames videoGames : games){
            System.out.println(videoGames);
        }

    }
}
