

public class VideoGames {

    private String name;
    private String developer;
    private String year;

    public VideoGames(String name, String developer, String year){
        this.name = name;
        this.developer = developer;
        this.year = year;
    }

    public String toString() {
        return "Name: " + name + "Developer: " + developer + "Year Released: " + year;
    }
}
